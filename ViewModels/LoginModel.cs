﻿using Microsoft.Build.Framework;

namespace JWTProyecto.ViewModels
{
    public class LoginModel
    {
        [Required]
        public string UsuarioCodigo { get; set; } = null!;
        [Required]
        public string Password { get; set; } = null!;
    }
}
