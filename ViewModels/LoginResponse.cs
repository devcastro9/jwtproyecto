﻿namespace JWTProyecto.ViewModels
{
    public class LoginResponse
    {
        public bool IsLogin { get; set; }
        public string? Token { get; set; }
    }
}
