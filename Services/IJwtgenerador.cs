﻿using JWTProyecto.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace JWTProyecto.Services
{
    public interface IJwtgenerador
    {
        public Task<string> CrearToken(UsuarioNet usuario, UserManager<UsuarioNet> userManager, IConfiguration config);
    }
}
