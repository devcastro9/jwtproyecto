﻿using JWTProyecto.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace JWTProyecto.Services
{
    public class JwtGenerador : IJwtgenerador
    {
        public async Task<string> CrearToken(UsuarioNet usuario, UserManager<UsuarioNet> userManager, IConfiguration config)
        {
            // Todos los roles
            IList<string> roles = await userManager.GetRolesAsync(usuario);
            // Claims
            List<Claim> claims = new()
            {
                new Claim(ClaimTypes.Sid, usuario.Id),
            };
            //if (roles.Count == 0)
            //{
            //    return "";
            //}
            Parallel.ForEach(roles, rol => claims.Add(new Claim(ClaimTypes.Role, rol)));
            // Token
            SymmetricSecurityKey securityKey = new(Encoding.UTF8.GetBytes(config["Jwt:Key"]!));
            SigningCredentials credentials = new(securityKey, SecurityAlgorithms.HmacSha256Signature);
            JwtSecurityToken tokenDescriptor = new(
                issuer: config["Jwt:Issuer"],
                //audience: config["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: credentials);

            string jwt = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

            return jwt;
        }
    }
}
