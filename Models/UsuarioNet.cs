﻿using Microsoft.AspNetCore.Identity;

namespace JWTProyecto.Models
{
    public class UsuarioNet : IdentityUser
    {
        public int DepartamentoId { get; set; }
    }
}
