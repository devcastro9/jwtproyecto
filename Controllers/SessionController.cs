﻿using JWTProyecto.Models;
using JWTProyecto.Services;
using JWTProyecto.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace JWTProyecto.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SessionController : ControllerBase
    {
        private readonly UserManager<UsuarioNet> _userManager;
        private readonly SignInManager<UsuarioNet> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IJwtgenerador _jwtgenerador;
        private readonly ILogger<SessionController> _logger;

        public SessionController(UserManager<UsuarioNet> userManager, SignInManager<UsuarioNet> signInManager, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, IJwtgenerador jwtgenerador, ILogger<SessionController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _contextAccessor = httpContextAccessor;
            _jwtgenerador = jwtgenerador;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<LoginResponse>> Login(LoginModel model)
        {
            UsuarioNet? usuario = await _userManager.FindByNameAsync(model.UsuarioCodigo);
            if (usuario == null)
            {
                return Unauthorized(new LoginResponse() { IsLogin = false, Token = "" });
            }
            SignInResult result = await _signInManager.CheckPasswordSignInAsync(usuario, model.Password, false);
            if (result.Succeeded)
            {
                string TokenGenerado = await _jwtgenerador.CrearToken(usuario, _userManager, _configuration);
                return Ok(new LoginResponse() { IsLogin = true, Token = TokenGenerado });
            }
            return Unauthorized(new LoginResponse() { IsLogin = false, Token = "" });
        }

        //[AllowAnonymous]
        [HttpGet("Usuarios")]
        public async Task<ActionResult<IEnumerable<UsuarioNet>>> GetUsuarios()
        {
            return await _userManager.Users.AsNoTracking().ToListAsync();
        }

        [AllowAnonymous]
        [HttpGet("Usuario/{id}")]
        public async Task<ActionResult<UsuarioNet>> GetUsuario(string id)
        {
            UsuarioNet? result = await _userManager.FindByIdAsync(id);
            if (result == null)
            {
                return NotFound();
            }
            await _userManager.AddPasswordAsync(result, "User123*");
            return result;
        }

        [AllowAnonymous]
        [HttpPost("Usuario")]
        public async Task<ActionResult<UsuarioNet>> CrearUsuario(UsuarioNet usuario)
        {
            IdentityResult result = await _userManager.CreateAsync(usuario, "User123*");
            if (!result.Succeeded)
            {
                return BadRequest();
            }
            return CreatedAtAction("GetUsuario", new { id = usuario.Id }, usuario);
        }

        [AllowAnonymous]
        [HttpGet("Me")]
        public async Task<ActionResult<string>> GetData()
        {
            await Task.Delay(100);
            if (_contextAccessor.HttpContext == null)
            {
                return BadRequest();
            }
            string? userName = _contextAccessor.HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid)?.Value;
            if (userName == null)
            {
                return NotFound();
            }
            return Ok(userName);
        }
    }
}
