﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace JWTProyecto.Data.Migrations
{
    /// <inheritdoc />
    public partial class ModificaciondeUsuario : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DepartamentoId",
                table: "AspNetUsers",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepartamentoId",
                table: "AspNetUsers");
        }
    }
}
