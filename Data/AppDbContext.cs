﻿using JWTProyecto.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace JWTProyecto.Data;

public partial class AppDbContext : IdentityDbContext<UsuarioNet>
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

    public virtual DbSet<Departamento> Departamentos { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Metodo base
        base.OnModelCreating(modelBuilder);
        // Api fluent
        modelBuilder.Entity<Departamento>(entity =>
        {
            entity.Property(e => e.Nombre).HasDefaultValueSql("''");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
